import 'package:flutter/material.dart';

const headBgColor = Colors.blueGrey;
const pageBgColor = Colors.grey;
const IconColor = Colors.redAccent;

const headerH1 = TextStyle(fontSize: 25, fontWeight: FontWeight.bold);
